# Manual Installation Steps
## Connect to Wifi
- See the [ArchWiki article on iwd](https://wiki.archlinux.org/title/Iwd#iwctl)

## archiso Updates & Pacman Modifications
1. Make the following changes to `/etc/pacman.conf`
    - Uncomment `ParallelDownloads = 5` to download packages much faster

## Partition & Mount the Disks
1. Use `cfdisk` to create the partitions, since it has an easy-to-use TUI
    - Use GPT
    - Create a 512 MiB or 1 GiB partition and select the type to be `EFI System`,
        - 1 GiB might be needed if there's a desire to use UKIs and/or mounting to `/boot` instead of `/efi`
    - Create a second partition for the remainder of the disk and set the type to be `Linux filesystem`
        - A single partition is more than sufficient for BTRFS using subvolumes and either ZRAM or a swapfile on a swap subvolume
        - Other setups have different needs
1. Format each partition
    - EFI System: `mkfs.fat -F 32 /dev/efi_system_partition`
    - BTRFS: `mkfs.btrfs -L mylabel /dev/partition`
1. Mount the BTRFS partition and set up the subvolumes with a flat layout for use with `snapper`, similar to the [suggested layout][https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout]
    - Mount the disk with `mount /dev/partition /mnt`
    - Create the `/` subvolume with `btrfs subvolume create /mnt/@`
        - This will later be mounted to `/` instead of the top-level subvolume
    - Create the `/home` subvolume with `btrfs subvolume create /mnt/@home`
        - Always a good idea to keep `/home` out of snapshots and have it separated
    - Create the `/.snapshots` subvolume with `btrfs subvolume create /mnt/@snapshots`
        - This will contain the snapshots for the `@` subvolume, ignoring any other subvolumes
    - Create the `/srv` subvolume with `btrfs subvolume create /mnt/@srv`
        - Contents don't need to be snapshotted and this can [prevent slowdowns](https://wiki.archlinux.org/title/Snapper#Preventing_slowdowns)
    - Create the `/var/log` subvolume with `btrfs subvolume create /mnt/@log`
        - Want to keep the logs to review why a system was messed up after restoring a snapshot!
        - Typically included as a subvolume for other offshoots (e.g., endeavourOS, Garuda, and the archinstall script)
    - Create the `/var/spool` subvolume with `btrfs subvolume create /mnt/@spool`
        - Contents don't need to be snapshotted and is included in an [ArchWiki Tip](https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout)
    - Create the `/var/tmp` subvolume with `btrfs subvolume create /mnt/@tmp`
        - Contents don't need to be snapshotted and this can [prevent slowdowns](https://wiki.archlinux.org/title/Snapper#Preventing_slowdowns)
    - Create the `/var/cache/pacman/pkg` subvolume with `btrfs subvolume create /mnt/@pacman_cache`
        - Contents don't need to be snapshotted and this can [prevent slowdowns](https://wiki.archlinux.org/title/Snapper#Preventing_slowdowns)
        - Typically included as a subvolume for other offshoots (e.g., endeavourOS, Garuda, and the archinstall script)
    - Create the `/var/lib/docker` subvolume with `btrfs subvolume create /mnt/@docker`
        - Contents don't need to be snapshotted, can cause snapshots to bloat quite a bit, and is included in an [ArchWiki Tip](https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout)
    - Create the `/var/lib/libvirt` subvolume with `btrfs subvolume create /mnt/@libvirt`
        - Contents don't need to be snapshotted, can cause snapshots to bloat quite a bit, and is included in an [ArchWiki Tip](https://wiki.archlinux.org/title/Snapper#Suggested_filesystem_layout)
    - Unmount the the disk with `umount /mnt`
1. Mount the partitions
    - BTRFS subvolumes: @ (Root) and others above:
        - `mount --mkdir -o subvol=@,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2             /dev/partition /mnt`
        - `mount --mkdir -o subvol=@home,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2         /dev/partition /mnt/home`
        - `mount --mkdir -o subvol=@snapshots,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2    /dev/partition /mnt/.snapshots`
        - `mount --mkdir -o subvol=@srv,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2          /dev/partition /mnt/srv`
        - `mount --mkdir -o subvol=@log,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2          /dev/partition /mnt/var/log`
        - `mount --mkdir -o subvol=@spool,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2        /dev/partition /mnt/var/spool`
        - `mount --mkdir -o subvol=@tmp,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2          /dev/partition /mnt/var/tmp`
        - `mount --mkdir -o subvol=@pacman_cache,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2 /dev/partition /mnt/var/cache/pacman/pkg`
        - `mount --mkdir -o subvol=@docker,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2       /dev/partition /mnt/var/lib/docker`
        - `mount --mkdir -o subvol=@libvirt,noatime,ssd,discard=async,compress-force=zstd:1,space_cache=v2      /dev/partition /mnt/var/lib/libvirt`
        - _Tip:_ Use `subvol=<path>` instead of `subvolid=<subvolid>` since restoring may change the IDs.
        - _NOTE:_ These options ensure SSD improvements are added, reduce SSD writes due to atime, attempt to force compression which has been shown to improve performance, and uses zstd:1 so it doesn't default to zstd:3
        - _NOTE:_ `space_cache=v2` & `discard=async` are on by default for supported devices, so they can maybe be removed [BTRFS Docs](https://btrfs.readthedocs.io/en/latest/Administration.html#btrfs-specific-mount-options)
        - _NOTE:_ Truly only the first mount needs most of the  [BTRFS Docs](https://btrfs.readthedocs.io/en/latest/Administration.html#btrfs-specific-mount-options)
    - EFI System: `mount --mkdir -o defaults,noatime /dev/efi_system_partition /mnt/efi`

## Base System Packages & fstab Generation
1. Install the simple base system with pacstrap: `pacstrap -K /mnt base base-devel linux linux-headers linux-firmware sudo intel-ucode git bash-completion zsh grml-zsh-config nano btrfs-progs networkmanager man-db`
    - **Arch Base**: `base`
    - **Arch Development Tools**: `base-devel`
    - **Arch Kernel**: `linux`
    - **Arch Kernel Headers**: `linux-headers`
    - **Linux Firmware**: `linux-firmware`
    - **Sudo capabilities**: `sudo`
    - **Intel Microcode**: `intel-ucode`
    - **Git**: `git`
    - **Bash Completion**: `bash-completion`
    - **Zsh**: `zsh`
    - **Good Zsh Defaults**: `grml-zsh-config`
    - **Text Editor**: `nano`
    - **BTRFS Tools**: `btrfs-progs`
    - **Network Manger**: `networkmanager`
    - **Man Pages**: `man-db`
    - _NOTE:_ More packages can be installed here, but I like to just set up some basics and finish after arch-chrootd environment go here, but it does seem nice to have
1. Generate Fstab with `genfstab -U /mnt >> /mnt/etc/fstab`

## Finalize Install with arch-chroot
1. Edit `/mnt/etc/environment` and set `EDITOR=nano` (and any other environment variables) so it takes effect with the chroot
1. Chroot to the new install with `arch-chroot /mnt`

### Timeizone, Clocks, and Localization
1. Set the timezone with `ln -sf /usr/share/zoneinfo/Region/City /etc/localtime`
1. Set the HW Clock with `hwclock --systohc`
1. Generate the locale by editing `/etc/locale.gen`, uncommenting `en_US.UTF-8`, and then running `local-gen`
1. Create `/etc/locale.conf` and add:
    ``` plaintext
    LANG=en_US.UTF-8
    LC_ADDRESS=en_US.UTF-8
    LC_IDENTIFICATION=en_US.UTF-8
    LC_MEASUREMENT=en_US.UTF-8
    LC_MONETARY=en_US.UTF-8
    LC_NAME=en_US.UTF-8
    LC_NUMERIC=en_US.UTF-8
    LC_PAPER=en_US.UTF-8
    LC_TELEPHONE=en_US.UTF-8
    LC_TIME=en_US.UTF-8
    ```
1. Create `/etc/vconsole.conf`
    1. Add the keymap with `KEYMAP=us`
    1. Add a font with `FONT=<fontname>` (e.g., `eurlatgr`)

### Hostname
1. Set the hostname in `/etc/hostname`
1. Update the `/etc/hosts` file to contain
    ``` plaintext
    127.0.0.1   localhost
    ::1         localhost
    127.0.1.1   <hostname>.localdomain   <hostname>
    ```

### Generate Initramfs
- Generate the intramfs image with `mkinitcpio -P`
    - _NOTE:_ It's easiest to just use `mkinitcpio` at this point, and either configure it or set up `dracut` after rebooting.

### Users
1. Set the editor to nano (for use with `visudo`) with `export EDITOR=nano`
1. Set the root password with `passwd`
1. Enable sudo priveleges through `visudo`
1. Create a user
    - Run `useradd -m -G wheel username`
    - Create a new user password with `passwd username`
1. Change the user's shell to `zsh` with `chsh -s /usr/bin/zsh username`

### Networking
- Enable NetworkManger with `systemctl enable NetworkManager.service`

### Install Grub
1. Install `grub`, `efibootmgr`, and `os-prober`
1. Install/set up grub with `grub-install --target=x86_64-efi --efi-directory=/efidir --boot-directory=/bootdir --bootloader-id=GRUB`
    - When using `/efi` and `/boot` this is: `grub-install --target=x86_64-efi --efi-directory=/efi --boot-directory=/boot --bootloader-id=GRUB`
1. Edit `/etc/defauts/grub`
    1. Add  `modprobe.blacklist=nouveau rd.driver.blacklist=nouveau nvidia-drm.modeset=1` to GRUB_CMDLINE_LINUX
    1. Add  `nowatchdog` to GRUB_CMDLINE_LINUX
1. Regenerate the grub config with `grub-mkconfig -o /boot/grub/grub.cfg`

### Fix fstab
- The generated `/etc/fstab` file contains the BTRFS mount option with `subvolid=<subvolid>`
- Remove this option, leaving just `subvol=<path>`

### Finishing
The base system is installed at this point, so exit the chroot environment and reboot!

# Post-install Configuration
## First Steps - Basics
### Time Synchronization
- Start the time synchronization service (no need for `ntp`) with `systemctl enable --now systemd-timesyncd.service`
    - The default settings can be seen in `/etc/systemd/timesyncd.conf`

### Mirrors & Pacman Settings
1. Make the following changes to `/etc/pacman.conf`
    - Uncomment `Color`
    - Add `ILoveCandy` for some fun pac-man graphics
    - Uncomment `ParallelDownloads = 5` to download packages much faster
    - Enable `multilib` repo if any multilib packages will be installed
1. Update mirrors
    - Install the `reflector` package
    - Open `/etc/xdg/reflector/reflector.conf`
    - Use country codes `us`
    - Set latest to 20 to use the latest mirrors
    - Set the sort to use `rate`
    - Set the new file to be `/etc/pacman.d/mirrorlist.pacnew`
    - Since there's an issue with the reflector service, run it manually for now
        - Back up the old `/etc/pacman.d/mirrorlist`
        - Run `reflector` manually with `reflector --save /etc/pacman.d/mirrorlist --protocol https --country us --latest 20 --sort rate`
        - Inspect the `/etc/pacman.d/mirrorlist` before continuing
    - _TODO:_ The following is broken at this point of the install
        - Make the directory `/run/systemd/resolve/`
        - Link the `stub-resolve.conf` to `/etc/resolve.conf` (or vice-versa?)
        - Enable & start the reflector service with `sudo systemctl enable --now reflector.service`
1. Update the system with `pacman -Syu`
1. Reboot

## Desktop Environment
### Pipewire Audio
1. Install the base `pipewire` and `lib32-pipewire` packages [https://wiki.archlinux.org/title/PipeWire]
1. Install `wireplumber` as the manager, with `--asdeps` so it's a dependency of pipewire
1. Install `pipewire-audio` for audio support, with `--asdeps` so it's a dependency of pipewire
1. Install `pipewire-alsa` to route apps using ALSA through PipeWire, with `--asdeps` so it's a dependency of pipewire
1. Install `pipewire-pulse` to route PulseAudio through PipeWire (including bluetooth), with `--asdeps` so it's a dependency of pipewire
    - See also [https://wiki.archlinux.org/title/Bluetooth_headset]
1. Install `pipewire-jack` and `lib32-pipewire-jack` to route apps using JACK through PipeWire, with `--asdeps` so it's a dependency of pipewire
1. Install Alsa/Sound firmware with `alsa-firmware` and `sof-firmware`
1. Install Alsa utils with `alsa-utils`

### Xorg
1. Install base Intel graphics drivers `mesa` and `lib32-mesa`
1. Install `vulkan-intel`, `lib32-vulkan-intel`, `vulkan-mesa-layers`, and `lib32-vulkan-mesa-layers` with `--asdeps`
1. Install the `xorg-server` package

### SDDM & KDE
1. Install `sddm` and enable with `sudo systemctl enable sddm`
1. Install a small KDE baseline with `plasma-meta konsole`
    - Select `phonon-qt5-gstreamer` when prompted (option 1)
1. Add other plasma tools
    - **File Archiver**: `ark`
    - **Wayland Support**: `egl-wayland`, `plasma-wayland-session`
    - **File Manager**: `dolphin`
        - Add thumbnail support with `ffmpegthumbs`, `libheif`, `kimageformats`, `kdegraphics-thumbnailers` with `--asdeps`
    - **Document Viewer**: `okular`
    - **Phone Connection**: `kdeconnect`
    - **Image Viewer**: `gwenview`
        - Add support for other formats with `qt5-imageformats`, and `kimageformats` with `--asdeps`
    - **MSPaint Alternative**: `kolourpaint`
    - **Screenshots**: `spectacle`
    - **Calculator**: `kcalc`
    - **Speed up launching KDE apps**: `kinit`
    - **Changing GTK Setttings**: `xsettingsd`
    - **Printing** - `print-manager`

## ZRAM
1. Install zram-generator with `sudo pacman -S zram-generator`
1. Disable zswap by switching to root and running `echo 0 > /sys/module/zswap/parameters/enabled`
1. Set up the config file
    1. Open `/etc/systemd/zram-generator.conf` and set the following (similar to Fedora's defaults):
        ``` plaintext
        [zram0]
        zram-size = min(ram / 2, 8192)
        compression-algorithm = zstd
        swap-priority = 100
        fs-type = swap
        ```
1. Reload all daemons with `systemctl daemon-reload`
1. Start ZRAM with `systemctl start systemd-zram-setup@zram0.service`
1. ZRAM status can be checked by running `zramctl`
1. To optimize ZRAM, edit `/etc/sysctl.d/99-vm-zram-parameters.conf` and add:
    ``` plaintext
    vm.swappiness = 180
    vm.watermark_boost_factor = 0
    vm.watermark_scale_factor = 125
    vm.page-cluster = 0
    ```
    - _NOTE: If these are added incorrectly, re-build initrd with dracut or mkinitcpio to get rid of errors during boot._
1. [ArchWiki Guide](https://wiki.archlinux.org/title/Zram#Using_zram-generator)
1. [ArchWiki Optimization Tips](https://wiki.archlinux.org/title/Zram#Optimizing_swap_on_zram)

## BTRFS Snapshots, Snapper, and BTRFS-Assistant
These steps will set up snapper similar to the openSUSE implementation, and create a wrapper around pacman.
1. Install `snapper`
1. Install `inotify-tools` to allow autostarting on filesystem events
1. Install `grub-btfs` for bootable snapshots in grub
1. Install `snap-pac` for pacman hooks to create pre/post snapshots on each transaction
1. Install `btrfs-assistant` and `btrfsmaintenance` from the AUR for helpful GUIs to handle snapshots and btrfs tools
1. Create a configuration with snapper
    - Unmount snapshots with `sudo umount /.snapshots`
    - Remove the current snapshots dir with `sudo rm -r /.snapshots`
    - Create a new config with `sudo snapper -c root create-config /`
    - Delete the new subvolume created by snapper `sudo btrfs subvolume delete /.snapshots`
    - Re-create the snapshots directory with `sudo mkdir /.snapshots`
    - Re-mount the snapshots directory (or just reboot) with something like `mount -a` if it's already in fstab
    - Change the permissions on the snapshots folder with `sudo chmod 750 /.snapshots`
1. Use `btrfs-assistant` to configure the snapshot settings
1. Start and enable the `grub-btrfsd` daemon
1. Run `sudo grub-mkconfig -o /boot/grub/grub.cfg` to get the snapshots in boot
1. Ensure that the `subvolID=<number>` parameter is not part of the FSTAB entries or `btrfs-assistant` will not work as expected. Instead, the `subvol=<name>` is perfectly fine
1. Set up properly booting into readonly as an overlay filesystem, which allows booting as normal
    - If using `dracut`:
        1. Edit `/etc/defaults/grub-btrfs/config`
        1. Uncomment the line containing `GRUB_BTRFS_SNAPSHOT_KERNEL_PARAMETERS`
    - If using `mkinitcpio`:
        1. Edit `/etc/mkinitcpio.conf`
        1. Add `grub-btrfs-overlayfs` to the end of `HOOKS=(*)`
        1. Regenerate the initramfs with `mkinitcpio -P`
    - See [the repo documentation](https://github.com/Antynea/grub-btrfs/tree/master/initramfs) for more details
1. After changing the config, re-generate the `grub-btrfs` menu with `sudo /etc/grub.d/41_snapshots-btrfs`
1. Update `/etc/btrfs-assistant.conf` and add `root="@snapshots,@,<UUID>"` under the `[Subvol-Mapping]` section.

## dracut (optional)
1. Install the `dracut` package
1. Generate intramfs with `dracut --hostonly --no-hostonly-cmdline /boot/initramfs-linux.img`
1. Generate intramfs fallback with `dracut /boot/initramfs-linux-fallback.img`
1. Make the directory for the hooks `sudo mkdir /etc/pacman.d/hooks`
1. Add the dracut hooks from the [ArchWiki](https://wiki.archlinux.org/title/Dracut#Generate_a_new_initramfs_on_kernel_upgrade) and stop the mkinitcpio hooks from this article (probably want to stop the actual hooks though)
1. Make sure the scripts are marked executable (e.g., `sudo chmod +x /usr/local/bin/dracut-install.sh` and `sudo chmod +x /usr/local/bin/dracut-remove.sh`)
1. Remove mkinitcpio with `sudo pacman -Rs mkinitcpio` and delete the config with `sudo rm /etc/mkinitcpio.conf.pacsave`
    - Also should remove `/etc/mkinitcpio.d` unless planning on using it again

## dkms
1. Install `dkms`
    - Generally, the existing pacman hooks should be sufficient for `dracut`, and `dkms` is run on each relevant package/kernel change!

### mkinitcpio
1. Create `/etc/pacman.d/hooks`
1. Create a new file `/etc/pacman.d/hooks/90-mkinitcpio-dkms.hook` and add the contents of either of the hooks below:
    - Add the hook from the [ArchWiki](https://wiki.archlinux.org/title/Dynamic_Kernel_Module_Support#Initial_ramdisk)
    - Or could add the [Nvidia Hook](https://wiki.archlinux.org/title/NVIDIA#pacman_hook)
1. Add `nvidia-dkms`, `linux-g14`, `xone-dkms`, and `xpadneo-dkms` targets for later usage.

## Bluetooth
1. Install `bluez-utils` (`bluez` should already be present)
    - Enable bluetooth with `systemctl enable bluetooth.service`

## Network Setup
### Firewall
1. Install the `firewalld` package
1. Enable the firewall with `sudo systemctl enable --now firewalld.service`
1. Install `iptables-nft`, replacing `iptables`, with `--asdeps`
1. Port 5353 needs to be open for `avahi` - should only do on home network only
1. _TODO:_ Configure Firewall based on personal preferences

### DNS
1. Install `dnsmasq`
1. _TODO:_ Configure dnsmasq based on personal preferences
1. _TODO:_ Set up dns caching
1. _TODO:_ Set up dns forwarding/third party
1. _TODO:_ Add `interface=lo` to `/etc/dnsmasq.`?
1. _TODO:_ Add `bind-interfaces` to `/etc/dnsmasq.`?

### nss-mdns
1. **Local hostname resolution w/ Avahi**: `nss-mdns` (install with `--asdeps`)
1. Open `/etc/nsswitch.conf` and add `mdns_minimal [NOTFOUND=return]` to the `hosts` line, before `resolve`
1. [ArchWiki Instructions](https://wiki.archlinux.org/title/avahi#Hostname_resolution)

1. _TODO:_ Figure out how to set this up

### Other
1. _TODO:_ Disable MulticastDNS in `/etc/systemd/resolved/conf`?
1. _TODO:_ Disable LLMR in `/etc/systemd/resolved/conf`?
1. _TODO:_ Enable IPv6PrivacyExtensions in `/etc/systemd/networkd.conf`?

### VPN
1. Install `networkmanager-openvpn` with `--asdeps` for OpenVPN support in NetworkManager
1. _TODO:_ Set up NordVPN connection according to the [ArchWiki](https://wiki.archlinux.org/title/NordVPN#Alternative_Method_:_connecting_to_NordVPN_using_NetworkManager)

### Encrypted WiFi Passwords
1. _TODO:_ Set up encrypted Wi-Fi passwords with [https://wiki.archlinux.org/title/NetworkManager#Encrypted_Wi-Fi_passwords]

## AUR Helper
1. Install `yay` as an AUR helper
    - Clone the repo with `git clone https://aur.archlinux.org/yay.git`
    - Install with `makepkg -si`

## Basic system tools
- **Zip/Archive**: `unrar unzip`
- **Neofetch**: `neofetch`
- **Pacman/Arch/Mirror Scripts & Helpers**: `pacutils pacman-contrib archlinux-contrib`
- **NVME Support**: `nvme`
- **USB Utilities**: `usbutils`
- **Find files**: `mlocate` or `plocate`
- **Fonts**: `noto-fonts`
- **File Copying**: `rsync`
- **Resource Monitor**: `htop`
- **Nano syntax highlighting** (install with `--asdeps`): `nano-syntax-highlighting`
- **HW Info**: `inxi` and `hwinfo`
- **Flatpak**: `flatpak` and then `flatpak-kcm` as a dependency to modify permissions in KDE
- **Power Management**: `power-profiles-daemon`
- **File Downloading**: `wget`
- **POSIX Man Pages**: `man-pages`
- **Log Rotation Tool**: `logrotate`
    - Default settings are pretty good
- **Nice du replacement**: `duf`
- **Printing/Scanning**: `cups`, `sane`
    - Install `cups-pdf` to allow printing to PDF
    - Install `hplip` for HP drivers
    - Install `python-pyqt5` for the `hplip` frontend
    - Install `python-pillow` for the HP Scan Utility according to the [ArchWiki](https://wiki.archlinux.org/title/SANE/Scanner-specific_problems#HP)
- **Backup Utility**: `fsarchiver`
- **See which package owns files**: `pkgfile`
- **Quick Man Parsing**: `tldr`
- **Find orphaned/duplicated/empty files**: `rmlint`
    - [ArchWiki Article](https://wiki.archlinux.org/title/System_maintenance#Old_configuration_files)
- **Extract Info About Packages**: `expac`
- **Package Rebuild Check**: `rebuild-detector`
    - Check which packages need to be rebuilt (e.g., with new libraries) using `checkrebuild` or `checkrebuild -v`
- **Discord**: `discord`
    - Add `libappindicator-gtk3` for systray support (with `--asdeps`)
- **Spellchecker**: `hunspell-en_US`
    - _TODO:_ is this right?
- **Web Browser**: `firefox`
- **Video Player**: `vlc`
    - `libva-vdpau-driver` and `lib32-libva-vdpau-driver` adds the vdpau backend for nvidia
        - _TODO:_ Validate that `mesa-vdpau` and `lib32-mesa-vdpau` too
    - `libva-intel-driver` and `lib32-libva-intel-driver` adds the vdpau backend for intel

## Configure Nano
1. Open `/etc/nanorc`
1. Uncomment/set the follwing lines:
    - `set atblanks` to line wrap at whitespace
    - `set autoindent` to indent lines
    - `set linenumbers` to enable line line numbers
    - `set mouse` to enable mouse support
    - `set smarthome` for better home key usage
    - `set softwrap` to enable line wrapping
    - `set tabsize 4` to use tabs as 4 spaces
    - `set trimblanks` to remove whitespace at the end of a line
    - `set zap` to allow deleting a highlighted area
    - `include "/usr/shahre/nano-syntax-highlighting/*.nanorc"` to enable syntax highlighting
        - There's a cuurrently a bug in the `nanorc.nanorc` file, detailed on the [ArchWiki](https://wiki.archlinux.org/title/Nano#Syntax_highlighting)
        - Fix with `sed -i 's/icolor brightnormal/icolor normal/g' /usr/share/nano-syntax-highlighting/nanorc.nanorc`
    - `extendsyntax python tabgives " "` to set spaces instead of tabs when editing Python
    - `extendsyntax makefile tabgives " "` to allow TABs when using makefiles
1. Uncomment the colors for fun
1. Enable the bind keys for uppercasing/lowercasing

## Finding Orphaned/Dated Packages
-  Add a package-query script to check for orphaned and of date packages using the [ArchWiki suggestion](https://wiki.archlinux.org/title/System_maintenance#Check_for_orphans_and_dropped_packages):
    ``` sh
    #!/bin/sh

    url='https://aur.archlinux.org/rpc?v=5&'
    pacman -Qmq | sort >| /tmp/pkgs

    curl -s "${url}type=info$(printf '&arg[]=%s' $(cat /tmp/pkgs))" \
        | jq -r '.results[]|.Name' | sort | comm -13 - /tmp/pkgs
    ```

## Gstreamer Codecs & Media Support
- Install all of the following packages with `--asdeps`
- **64-bit Gstreamer plugins**:  `gst-plugin-pipewire gst-plugins-base gst-plugins-bad gst-plugins-good gst-plugins-ugly gst-libav`
- **32-bit Gstreamer plugins**:  `lib32-gst-plugins-base lib32-gst-plugins-base-libs lib32-gst-plugins-good`
    - `lib32-gst-plugins-base-libs` is installed as a dependency to `lib32-gst-plugins-base`
    - _NOTE:_ The 32-bit plugins might not be necessary, but they can help with older games!
- **MP3 Metadata**: `libid3tag`
- **Encrypted DVDs**: `libdvdcss`
- **RAW Images (Camera)**: `libopenraw`

## Filesystem Support
- **F2FS for Flash**: `f2fs-tools`
- **DOS Filesystem**: `dosfstools`
- **FAT grub-mkrescue**: `mtools`
- **exFAT Support**: `` _TODO:_ Do I need?

## Fonts
1. **Firacode Nerd Font** - `ttf-firacode-nerd`
1. **Noto Emojis**: `noto-fonts-emoji` (use `--asdeps`)
1. **Microsoft Fonts (AUR)** - `ttf-ms-win11-auto` or `ttf-ms-win10-auto` _TODO:_ Seems broken?
1. **DejaVu TTF**: `ttf-dejavu` _TODO:_ If I want?

## Install Nvidia Drivers
1. Install `nvidia-dkms` to get the dkms drivers
    - `nvidia-utils` should be installed as a dependency
1. Install `lib32-nvidia-utils` to get the 32-bit utilities (with `--asdeps`)
1. If using `mkinitcpio`:
    1. Edit `/etc/mkinitcpio.conf`
    1. May need to add `i915` and/or `nvidia nvidia_modeset nvidia_uvm nvidia_drm` to `MODULES`
1. Update grub
    1. Edit `/etc/default/grub`
    1. Ensure `modprobe.blacklist=nouveau rd.driver.blacklist=nouveau nvidia-drm.modeset=1` was added to GRUB_CMDLINE_LINUX
    1. Regenerate the grub config with `grub-mkconfig -o /boot/grub/grub.cfg`
* **Source:** https://asus-linux.org/wiki/arch-guide/

## Steam/WINE/Lutris
1. Install all necessary drivers
    1. Ensure nvidia drivers are already installed
    1. Ensure Vulkan drivers are already installed `vulkan-icd-loader lib32-vulkan-icd-loader`
    1. Ensure Vulkan drivers for nvidia drivers are already installed `nvidia-utils lib32-nvidia-utils`
1. **Steam**: `steam`
1. **Protonup-qt**:`protonup-qt` (AUR or Flatpak)
1. **WINE**: `wine-staging` and `winetricks`
    - Install `kdialog` for a KDE GUI (with `--asdeps`)
1. **Lutris**:`lutris`
    - Install `vkd3d` and `lib32-vkd3d` for DirectX 12 support (with `--asdeps`)
1. **Mangohud** - `mangohud`
1. **Gamemode** - `gamemode` and `lib32-gamemode` with `--asdeps` since they're optional lutris dependencies
1. **VulkanD3D** - `vkd3d` and `lib32-vkd3d`with `--asdeps` since they're optional lutris dependencies
1. **Goverlay** - `yay -S goverlay-bin`

# Personal Settings/Home Directory
## KDE Personal Preferences and Settings:
1. Set the global theme to Breeze Dark
1. In **Input Devices -> Keyboard**: Enable numlock on plasma startup
1. In **Power Management -> Energy Saving**: Set up screen dim/display and power button settings. Personal preference:
    1. AC Power:    Dim = 10 min, Screen Off = 15 min, Power Button = Shutdown, Suspend: N/A
    1. Battery:     Dim = 5 min,  Screen Off = 10 min, Power Button = Shutdown, Suspend: Sleep after 15 min
    1. Low Battery: Dim = 1 min,  Screen Off = 2 min,  Power Button = Shutdown, Suspend: Sleep after 5 min
1. Enable double-click to open files/folders
    1. Go to Settings -> Workspace Behavior -> General Behavior
    1. Under `Clicking files or folders`, select the `Selects them` radio button
1. Update Dolphin to:
    1. **Show Hidden Files**
    1. **Sort Folders Before Files**
    1. **Sort Hidden Files Last**

## Misc/Personal Setup
1. Mount any external drives that weren't added during install and update fstab mount options
    1. Ensure that the `user` or `users` options are not present in fstab to prevent drives from being mounted with `noexec`
    1. For btrfs, `compress-force=zstd:1,ssd,space_cache=v2,commit=120` seems to be a good, recommended setup for NVMe SSDs.
    1. Discard is discouraged on NVMe SSDs (arch wiki), so a trim schedule should be set up.
    1. For ext4, `noatime,commit=120`
1. Enable bluetooth with `sudo systemctl enable --now bluetooth.service`
1. Make a symbolic link from the user GTK settings to global GTK settings for uniform appearance:
    1. `sudo ln -s /home/username/.gtkrc-2.0 /etc/gtk-2.0/gtkrc`
    1. `sudo ln -s /home/username/.config/gtk-3.0/settings.ini /etc/gtk-3.0/settings.ini`
    * **Source:** https://wiki.archlinux.org/title/GTK#Theme_not_applied_to_root_applications
1. Change headset/mic to **Pro Audio**
1. Create symbolic links for personal files (useful while distro-hopping!)
    1. Firefox Profile
        1. Remove the `/home/username/.mozilla/firefox/*-default-release` folder
        1. Run `ln -s <abs-path-to-profile> /home/username/.mozilla/firefox/*-default-release` using the same name as the previous step
        1. Example: `ln -s /mnt/storage/personal/ff_profile/ /home/beat/.mozilla/firefox/c9zggch2.default-release`
    1. Downloads
    1. Pictures
    1. Music
    1. Documents
    1. Any other folders (school/work/tools/repos/development/etc)

# Keyboard, Headset, Controllers, and Mice
## Set Up Xbox Controllers
1. Xone ([github](https://github.com/medusalix/xone))
    1. Install from AUR: `yay -S xone-dkms`
    1. Reboot the system.
1. Xpadneo ([github](https://github.com/atar-axis/xpadneo))
    1. Install from AUR: `yay -S xpadneo-dkms`
    1. Reboot the system.

## Corsair Keyboard
1. Install with AUR using `yay -S ckb-next`
1. Start ckb-next and configure the keyboard as desired.

## Corsair Wireless Headset
1. Install with AUR using `yay -S headsetcontrol`
    1. Reload udev rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
1. Install the headset notification tray icon/GUI from [github](https://github.com/centic9/headset-charge-indicator/)
    1. Clone the repo: `git clone https://github.com/centic9/headset-charge-indicator.git`
    1. Run `./install.sh` to auto-start

## Tools from asus-linux Project
* https://asus-linux.org/wiki/arch-guide/
1. Add the g14 repo keys:
    ``` bash
    pacman-key --recv-keys 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
    pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
    pacman-key --lsign-key 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
    pacman-key --finger 8F654886F17D497FEFE3DB448B15A6B0E9A3FA35
    ```
1. Add the g14 repo to `/etc/pacman.conf`
    * `[g14]`
    * `SigLevel = DatabaseNever Optional TrustAll`
    * `Server = https://arch.asus-linux.org`
1. Run a full system update with `sudo pacman -Syu`
1. Install asusctl with `sudo pacman -S asusctl`
1. Ensure power-profiles-daemon is enabled with `systemctl enable --now power-profiles-daemon.service`
1. Add the g14 kernel `linux-g14` and `linux-g14-headers`
1. Install `supergfxctl` or something else for dual GPUs

# Misc
1. Uninstall `modemmanager`
1. _TODO:_ Maybe change `CheckHostIP` to `yes` in `/etc/ssh/ssh_config`
1. _TODO:_ Maybe add the following to `/etc/hosts`
    ``` plaintext
    ::1        localhost ip6-localhost ip6-loopback
    ff02::1    ip6-allnodes
    ff02::2    ip6-allrouters
    ```
1. _TODO:_ Blacklist kernel modules that are unnecessary, like Nobara does:
    - `appletalk`, `atm`, `ax25`, `batman-adv`, `floppy`, `l2tp_eth`, `l2tp_ip`, `l2tp_netlink`, `netrom`, `nfc`, `rds`, `rose`, `sctp`, `v4l2loopback`, `vhost`
        - atm
1. _TODO:_ Increase vhost memory by adding `options vhost max_mem_regions=509` to `/etc/modprobe.d/vhost.conf`
1. Set up a pacman hook for keeping an up to date list of packages from [https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#List_of_installed_packages]

# Grub Theme
1. Add some fun themes
    -  **Breeze Theme for Grub**: `breeze-grub`,
        - Open `/etc/default/grub` and set `GRUB_THEME="/usr/share/grub/themes/breeze/theme.txt"`
        - Regenerate the grub config with `grub-mkconfig -o /boot/grub/grub.cfg`

## Enable SysReq Commands
1. Run `echo 'kernel.sysreq=1' | sudo tee /etc/sysctl.d/99-reisub.conf`
    1. Enables the SysReq key by default (`alt` + `Print Screen`)
    1. Allows resetting from a borked state by holdng `alt`, hitting the `Print Screen` key, then typing `REISUB`.
    1. Helpful mnemonic `Reboot Even If System Is Utterly Broken`
* **Source:** https://forum.endeavouros.com/t/tip-enable-magic-sysrq-key-reisub/7576

## Unnecessary Packages
- `pavucontrol` - seems like it's good to control sound?
    - _TODO:_ Probably don't need since it's for PulseAudio and it's not installed on Tumbleweed

---

# Archinstall Script
Install manually instead, since there are bugs with the script. This was done to compare the outcome.

## Installation Steps
1. Run the `archinstall` script
1. In **Mirrors**, set the **Mirror Region** to **United States**
1. In **Disk Configuration**, use the **"best-effort default"**
    1. Select the drive
    1. Select **btrfs**
    1. Use the default structure
    1. Enable compression
1. Change the **Bootloader** to **Grub**
1. Ensure **Swap** is enabled
1. Set the **hostname**
1. Set the **root** password
1. Create a user, set the password, and give sudo privileges through the prompt
1. In **Profile**, select **Desktop** and **KDE** along with the graphics drivers (start with **Intel**)
    - Desktop installs `nano vim openssh htop wget iwd wireless_tools wpa_supplicant smartmontools xdg-utils`
    - KDE installs `plasma-meta kwrite ark egl-wayland konsole dolphin plasma-wayland-session`
    - Intel installs `intel-media-driver mesa libvainteldriver intelmediadriver vulkanintel`
    - Greeter installs `sddm`
1. In **Audio Server** select **Pipewire**
1. In **Network configuration** select **Network Manager**
1. Set the **Timezone**
1. In **Additional Packages**, install `micro intel-ucode bash-compleion git meld texinfo man-db linux-headers`
1. In **Optional Repositories**, add `multilib`
1. Install!!
