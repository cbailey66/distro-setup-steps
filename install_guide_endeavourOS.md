# Installer:
1. When booting from the install image, you may need to add `modprobe.blacklist=nouveau` to ensure the nouveau drivers don't mess anything up
1. Everything is extremely straightforward with the installer, so the defaults work for most everything
1. Select KDE (or desktop of choice)
1. Add **zen** and **LTS** kernels, if desired. The former is for performance and the latter is for a safe backup

# Basic Post-install Steps
## First Boot Settings for Intel/Optimus laptops
1. On reboot, may need to add `modprobe.blacklist=nouveau` in the grub menu
1. Add it and `nvidia-drm.modeset=1` to the grub config and regenerate grub
1. There's a chance that `nvidia-prime` and/or `nvidia-utils` may need to be installed
    * My external monitors weren't working properly, no KDE icons showed up, and even the right-click context menus were extremely wonky. Administrator popups were blank and took up the entire screen...
    * Fixing this seemed to be a combination of `nvidia-dkms`, `nvidia-utils`, and `nvidia-prime`
    * Haven't looked further because it's finally working.

## BTRFS Snapshots, Snapper, and BTRFS-Assistant
These steps will set up snapper similar to the openSUSE implementation, and create a wrapper around pacman.
1. Install the snapper-support AUR package with `yay -S snapper-support`, which installs the dependencies from the official repos
1. Install btrfs-assistant from AUR with `yay -S btrfs-assistant`, which is a helpful gui for snapshots and btrfs scrubbing
1. Until 4.13 of `btrfs-grub` is released, a workaround is needed for `dracut`:
    1. Open the grub.d config in a text editor `sudo nano /etc/grub.d/41_snapshots-btrfs`
    1. Add the following to the Linux command line `rd.live.overlay.overlayfs=1`
    1. Ensure it's generated with `sudo /etc/grub.d/41_snapshots-btrfs`
    * **Source:** https://forum.endeavouros.com/t/configure-booting-read-only-snaphots-with-dracut/35178/

## Set up ZRAM
1. Install zram-generator with `sudo pacman -S zram-generator`
1. Disable zswap by switching to root and running `echo 0 > /sys/module/zswap/parameters/enabled`
1. Set up the config file
    1. The "easy" way is to use the Fedora defaults, done by `yay -S zram-generator-defaults`
    1. The file is set to `/usr/lib/systemd/zram-generator.conf`
    1. It sets the following:
        * `[zram0]`
        * `zram-size = min(ram, 8192)`
    1. The example file is on [github](https://github.com/systemd/zram-generator/blob/main/zram-generator.conf.example), and installed in documentation folders
    1. [AUR package](https://aur.archlinux.org/packages/zram-generator-defaults)
* **Source:** https://wiki.archlinux.org/title/Improving_performance#Zram_or_zswap

## Enable SysReq Commands
1. Run `echo 'kernel.sysreq=1' | sudo tee /etc/sysctl.d/99-reisub.conf`
    1. Enables the SysReq key by default (`alt` + `Print Screen`)
    1. Allows resetting from a borked state by holdng `alt`, hitting the `Print Screen` key, then typing `REISUB`.
    1. Helpful mnemonic `Reboot Even If System Is Utterly Broken`
* **Source:** https://forum.endeavouros.com/t/tip-enable-magic-sysrq-key-reisub/7576

## Install Nvidia Drivers
1. Uninstall any existing drivers with `sudo pacman -R nvidia-dkms`
1. Look for other driver-related stuff with `pacman -Qs nvidia`
1. Reinstall the drivers with `sudo pacman -S nvidia-dkms`
1. Install **nvidia-utils** with `sudo pacman -S nvidia-utils`
1. Add the Nvidia repo via yast (using the community repository option) or:
1. Add `modprobe.blacklist=nouveau rd.driver.blacklist=nouveau nvidia-drm.modeset=1` to GRUB_CMDLINE_LINUX in `/etc/default/grub`
1. Regenerate grub with `sudo grub-mkconfig -o /etc/grub.cfg`
* **Source:** https://asus-linux.org/wiki/arch-guide/

# Personal Settings/Home Directory
## KDE Personal Preferences and Settings:
1. Set the global theme to Breeze Dark
1. In **Input Devices -> Keyboard**: Enable numlock on plasma startup
1. In **Power Management -> Energy Saving**: Set up screen dim/display and power button settings. Personal preference:
    1. AC Power:    Dim = 10 min, Screen Off = 15 min, Power Button = Shutdown, Suspend: N/A
    1. Battery:     Dim = 5 min,  Screen Off = 10 min, Power Button = Shutdown, Suspend: Sleep after 15 min
    1. Low Battery: Dim = 1 min,  Screen Off = 2 min,  Power Button = Shutdown, Suspend: Sleep after 5 min
1. Enable double-click to open files/folders
    1. Go to Settings -> Workspace Behavior -> General Behavior
    1. Under `Clicking files or folders`, select the `Selects them` radio button
1. Update Dolphin to:
    1. **Show Hidden Files**
    1. **Sort Folders Before Files**
    1. **Sort Hidden Files Last**

## Misc/Personal Setup
1. Mount any external drives that weren't added during install and update fstab mount options
    1. Ensure that the `user` or `users` options are not present in fstab to prevent drives from being mounted with `noexec`
    1. For btrfs, `compress-force=zstd:1,ssd,space_cache=v2,commit=120` seems to be a good, recommended setup for NVMe SSDs.
    1. Discard is discouraged on NVMe SSDs (arch wiki), so a trim schedule should be set up.
    1. For ext4, `noatime,commit=120`
1. Enable bluetooth with `sudo systemctl enable --now bluetooth.service`
1. Make a symbolic link from the user GTK settings to global GTK settings for uniform appearance:
    1. `sudo ln -s /home/username/.gtkrc-2.0 /etc/gtk-2.0/gtkrc`
    1. `sudo ln -s /home/username/.config/gtk-3.0/settings.ini /etc/gtk-3.0/settings.ini`
    * **Source:** https://wiki.archlinux.org/title/GTK#Theme_not_applied_to_root_applications
1. Change headset/mic to **Pro Audio**
1. Create symbolic links for personal files (useful while distro-hopping!)
    1. Firefox Profile
        1. Remove the `/home/username/.mozilla/firefox/*-default-release` folder
        1. Run `ln -s <abs-path-to-profile> /home/username/.mozilla/firefox/*-default-release` using the same name as the previous step
        1. Example: `ln -s /mnt/storage/personal/ff_profile/ /home/beat/.mozilla/firefox/c9zggch2.default-release`
    1. Downloads
    1. Pictures
    1. Music
    1. Documents
    1. Any other folders (school/work/tools/repos/development/etc)

# Software
## KDE/Qt/DE Additions
1. **Plasma Browser Extension** - `sudo pacman -S plasma-browser-integration`
1. **Plasma System Monitor** - `sudo pacman -S plasma-systemmonitor`
1. **Plasma Rename Tool** - `sudo pacman -S krename`
1. **Plasma Wayland Support** - `sudo pacman -S plasma-wayland-session`
1. **Plasma Discover & Addons (for KDE Plasma Widgets/Themes/Etc.)** - `sudo pacman -S discover kdeplasma-addons`
1. **Plasma Addons (for KDE Plasma Widgets/Themes/Etc.)** - `sudo pacman -S discover`
1. **Wayland** - `sudo pacman -S wayland`
1. **Kvantum** - `sudo pacman -S kvantum`
1. **Partition Manager** - `sudo pacman -S partitionmanager`
1. **htop** - `sudo pacman -s htop`

## Shell Configuration and Helpers
1. **Fish** - `sudo pacman -s fish`
1. **Thefuck** - `sudo pacman -s thefuck`
1. **Starship** - `sudo pacman -s starship`

## Fonts
* Apparently **Noto Fonts Emoji** stopped Xorg from booting. Need to investigate.
1. **Noto Fonts Emoji** - `sudo pacman -S noto-fonts-emoji`
1. **Noto Fonts Extra** - `sudo pacman -S noto-fonts-extra`
1. **Firacode Nerd Font** - `sudo pacman -S ttf-firacode-nerd`

## Media/Video Settings
1. **MPV** - `sudo pacman -S mpv`
1. **OBS Studio** - `sudo pacman -S obs-studio`
    1. Start up to configure webcam and settings
1. **GIMP** - `sudo pacman -S gimp`

## Chat
1. **Discord** - `sudo pacman -S discord`

## Gaming
1. **Steam**
    1. Install Vulkan icd loaders with `sudo pacman -S --needed vulkan-icd-loader lib32-vulkan-icd-loader`
    1. Install Vulkan Nvidia/Intel Drivers with `sudo pacman -S --needed nvidia-utils lib32-nvidia-utils vulkan-intel lib32-vulkan-intel`
    1. Install steam itself `sudo pacman -S steam`
    * **Source:** https://discovery.endeavouros.com/gaming/gaming-101/2022/01/
    * **Source:** https://discovery.endeavouros.com/gaming/steam-lutris-wine/2022/01/
1. **Lutris** - `sudo pacman -S lutris`
1. **ProtonUp-QT** - `yay -S protonup-qt`
1. **Mangohud & Goverlay** - `yay -S goverlay-bin`

## Wine
1. **Wine Staging** - `sudo pacman -S wine-staging`
1. **Winetricks** - `sudo pacman -S winetricks`

## Web Browsing
Note that some webapps (e.g., telehealth) might only run on Linux through Chrome/Chromium
1. **Chromium** - `sudo pacman -S chromium`

## Development/Text Editing
1. **Micro** - `sudo pacman -S micro`
1. **VS Code** - `sudo pacman -S code`

## Paid software
1. **BeyondCompare**
  1. Install with `yay -S bcompare`
1. **NordVPN**
  1. Install with AUR `yay -S nordvpn-bin`
  1. Enable the service `sudo systemctl enable --now nordvpnd`
  1. Add user to the nordvpn group with `sudo usermod -aG nordvpn $USER`
  1. Add the NordVPN Plasma Widget!

## Tools from asus-linux Project
* https://asus-linux.org/wiki/arch-guide/
1. Uninstall the nvidia drivers if they were automatically installed, using `sudo pacman -R nvidia-dkms`
1. Add the following lines to the end of `/etc/pacman.conf`
    * `[g14]`
    * `SigLevel = DatabaseNever Optional TrustAll`
    * `Server = https://arch.asus-linux.org`
1. Run a full system update with `sudo pacman -Suy`
1. Install asusctl with `sudo pacman -S asusctl`
1. Ensure power-profiles-daemon is enabled with `systemctl enable --now power-profiles-daemon.service`
1. Install supergfxctl with `sudo pacman -S supergfxctl`
1. Add `nvidia-drm.modeset=1` to GRUB_CMDLINE_LINUX in `/etc/default/grub`
1. Regenerate grub with `sudo grub2-mkconfig -o /etc/grub2.cfg`

# Keyboard, Headset, Controllers, and Mice
## Set Up Xbox Controllers
1. Xone ([github](https://github.com/medusalix/xone))
    1. Install from AUR: `yay -S xone-dkms`
    1. Reboot the system.
1. Xpadneo ([github](https://github.com/atar-axis/xpadneo))
    1. Install from AUR: `yay -S xpadneo-dkms`
    1. Reboot the system.

## Corsair Keyboard
1. Install with AUR using `yay -S ckb-next`
1. Start ckb-next and configure the keyboard as desired.

## Corsair Wireless Headset
1. Install `headsetcontrol` from [github](git clone https://github.com/Sapd/HeadsetControl)
    1. Install Dependencies `sudo pacman -S --needed git cmake hidapi`
    1. Clone the repo `git clone https://github.com/Sapd/HeadsetControl && cd HeadsetControl`
    1. Make the build directory `mkdir build && cd build`
    1. Run cmake `cmake ..`
    1. Build `make`
    1. Install globally `sudo make install`
    1. Reload udev rules with `sudo udevadm control --reload-rules && sudo udevadm trigger`
1. Install the headset notification tray icon/GUI from [github](https://github.com/centic9/headset-charge-indicator/)
    1. Clone the repo: `git clone https://github.com/centic9/headset-charge-indicator.git`
    1. Run `./install.sh` to auto-start

## Razer Drivers
1. Razer Drivers
    1. Run `yay -S openrazer-meta`
    1. Run `sudo gpasswd -a $USER plugdev`
    1. Install RazerGenie with `yay -S razergenie`
