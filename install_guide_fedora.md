# Installer & Drive Formatting
## Existing BTRFS Partitions
1. Use the custom-blivet GUI option for partitioning.
1. Format existing `/boot/efi` partition.
    1. Ensure the size is **512-600 MiB**.
    1. Use the **EFI** filesystem.
    1. Set the mount point to `/boot/efi`.
1. Format existing `/boot` partition.
    1. Ensure the size is **1 GiB (1024 MiB)**.
    1. Use the **ext4** filesystem.
    1. Set the mount point to `/boot`.
1. Enter the **btrfs subvolume** view.
1. Delete any existing **timeshift** snapshots.
1. Format existing `root` subvolume.
    1. Enter the name `@` for the new subvolume if timeshift is desired
    1. Set the mount point to `/`.
1. Set the mountpoint for the existing `home` subvolume to `/home`
    1. Ensure that the name is `@home` if timeshift is desired.

## Clean Install
1. The main thing for timeshift is to use the names `@` for `/` and `@home` for `/home`

# Basic Post-install Steps
## KDE Personal Preferences and Settings:
1. Set up the desired monitor configuration and settings.
    * Ensure all monitors have the same refresh rate due to known bugs.
1. Set the global theme to Breeze Dark
1. In **Input Devices -> Keyboard**: Enable numlock on plasma startup
1. In **Power Management -> Energy Saving**: Set up screen dim/display and power button settings. Personal preference:
    1. AC Power:    Dim = 10 min, Screen Off = 15 min, Power Button = Shutdown, Suspend: N/A
    1. Battery:     Dim = 5 min,  Screen Off = 10 min, Power Button = Shutdown, Suspend: Sleep after 15 min
    1. Low Battery: Dim = 1 min,  Screen Off = 2 min,  Power Button = Shutdown, Suspend: Sleep after 5 min

## dnf.conf Settings
1. Open the file `/etc/dnf/dnf.conf` in a text editor (or `sudo nano /etc/dnf/dnf.conf`).
1. Set `max_parallel_downloads=10` for downloading packages in parallel (number can be lowered depending on internet connection).
1. Set `deltarpm=True` to allow small downloads and rebuilding rpm packages locally, which tends to have better performance.
1. Set `defaultyes=True` to change the default option to Yes if desired.

## Update the system
1. Update installed packages: `sudo dnf update --refresh`
1. Apply any upgrades: `sudo dnf upgrade`

## Add Snapper & BTRFS Utils
1. Run `sudo dnf install snapper btrfs-assistant python3-dnf-plugin-snapper`.
1. Start btrfs-assistant.
1. Set up a weekly (or preferred timeline) scrub schedule.
1. Create a configuration for root.
1. Set up the settings as desired for timeline snapshots.
1. Take the first manual snapshot.
1. Reboot for the dnf wrapper to apply.

## Other Settings
1. Create symbolic links for personal files (useful while distro-hopping!)
    1. Firefox Profile
        1. Remove the `/home/username/.mozilla/firefox/*-default-release` folder
        1. Run `ln -s <abs-path-to-profile> /home/username/.mozilla/firefox/*-default-release` using the same name as the previous step
        1. Example: `ln -s /mnt/storage/personal/ff_profile/ /home/beat/.mozilla/firefox/c9zggch2.default-release`
    1. Downloads
    1. Pictures
    1. Music
    1. Documents
    1. Any other folders (school/work/tools/repos/development/etc)

# Third-party Repos, Codecs, Drivers, and Software
## Third Party Repository Setup
1. Enable all of the rpmfusion release libraries with `sudo dnf install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm`
1. Install the appstream data with: `sudo dnf groupupdate core`
1. Enable flatpak with `flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`
1. Update flatpak with `flatpak update`

## Add Media Codecs/Libraries
1. `sudo dnf groupupdate multimedia --setop="install_weak_deps=False" --exclude=PackageKit-gstreamer-plugin --allowerasing`
1. `sudo dnf groupupdate sound-and-video`
1. `sudo dnf install gstreamer1-plugins-{bad-\*,good-\*,base} gstreamer1-plugin-openh264 gstreamer1-libav --exclude=gstreamer1-plugins-bad-free-devel ffmpeg gstreamer-ffmpeg`
1. `sudo dnf install lame\* --exclude=lame-devel`
1. `sudo dnf group upgrade --with-optional Multimedia`

## Install Other Useful Software
Use DNF to install useful software.
1. git (because it's not included by default...) - `sudo dnf install git`
1. Discord - `sudo dnf install discord`
1. Kvantum - `sudo dnf install kvantum`
1. MPV - `sudo dnf install mpv`
1. OBS Studio - `sudo dnf install obs-studio`
    1. Start up to configure webcam and settings
1. Steam -
    1. Install Vulkan icd loaders with `sudo pacman -S --needed vulkan-icd-loader lib32-vulkan-icd-loader`
    1. Install Vulkan Nvidia/Intel Drivers with `sudo pacman -S --needed nvidia-utils lib32-nvidia-utils vulkan-intel lib32-vulkan-intel`
    1. Install steam itself `sudo dnf install steam`
1. Lutris - `sudo pacman -S lutris`
1. Mangohud & Goverlay - `sudo dnf install goverlay`
1. Wine Stating - `sudo pacman -S wine-staging`
1. Winetricks - `sudo pacman -S winetricks`
1. The following software are on Flathub and can just be installed through Discover or Software.
    1. ProtonUp-QT
    1. Flatseal - `flatpak install Flatseal`
    1. VSCodium - `flatpak install vscodium`
    1. Ungoogled Chromium - `flatpak install ungoogled chromium`

## Install Paid Software
1. **BeyondCompare**
  1. Download the RPM with `wget https://www.scootersoftware.com/bcompare-4.4.4.27058.x86_64.rpm`
  1. Import the RPM with `sudo rpm --import https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware`
  1. Install with `sudo dnf install bcompare-4.4.4.27058.x86_64.rpm`
1. **NordVPN**
  1. Run `sh <(wget -qOnordvpn.sh https://downloads.nordcdn.com/apps/linux/install.sh)`
  1. Change the installer to executable `chmod +x nordvpn.sh`
  1. Install `sudo sh ./nordvpn.sh`
  1. Add user to the nordvpn group with `sudo usermod -aG nordvpn $USER`

## Install Nvidia Drivers
* https://asus-linux.org/wiki/fedora-guide/
1. Install the Kernel Development files `sudo dnf install kernel-devel`
1. Install the nvidia drivers `sudo dnf install akmod-nvidia xorg-x11-drv-nvidia-cuda`
1. Wait until `modinfo -F version nvidia` returns the driver version before moving to the next step
1. Enable the hibernate, suspend, and resume services `sudo systemctl enable nvidia-hibernate.service nvidia-suspend.service nvidia-resume.service`
1. Update the kernel command line parameters used with grub
    1. Open the grub configuration `sudo gedit /etc/default/grub`
    1. Ensure that `GRUB_CMDLINE_LINUX` contains `rd.driver.blacklist=nouveau modprobe.blacklist=nouveau nvidia-drm.modeset=1`
    1. Copies can be deleted - just needs to be present once
1. Update grub `sudo grub2-mkconfig -o /etc/grub2.cfg`
1. Reboot

## Tools from asus-linux Project
* https://asus-linux.org/wiki/fedora-guide/
* Kernel shouldn't be needed for older laptops
1. Enable the repo `sudo dnf copr enable lukenukem/asus-linux`
1. Install the programs  `sudo dnf install asusctl supergfxctl asusctl-rog-gui`
1. Refresh/update the system `sudo dnf update --refresh`
1. Enable supergfxd `sudo systemctl enable supergfxd.service`

# Keyboard, Headset, and Controllers
## Set Up Xbox Controllers
1. Xone (can build directly from github: https://github.com/medusalix/xone)
    1. This is easiest to do through the COPR: https://copr.fedorainfracloud.org/coprs/sentry/xone/
    1. Install dependencies `sudo dnf install dkms cabextract`
    1. `sudo dnf copr enable sentry/xone` to enable the repo.
    1. `sudo dnf install xone lpf-xone-firmware` to install the firmware.
    1. `lpf approve xone-firmware` to approve the firmware package (will prompt the user to add themselves to the pkg-build group).
    1. Log out and log back in for the above group addition to take place.
    1. `lpf build xone-firmware` to build the firmware package.
    1. `lpf install xone-firmware` to install the firmware package.
    1. Reboot the system.
1. Xpadneo (can build directly from github: https://github.com/atar-axis/xpadneo)
    1. This is easiest to do through the COPR: https://copr.fedorainfracloud.org/coprs/sentry/xpadneo/
    1. Enable the repo: `sudo dnf copr enable sentry/xpadneo`.
    1. Install the package: `sudo dnf install xpadneo`.

## Set Up Headset, Mouse, and Keyboard
1. Corsair Keyboard (**ckb-next**)
    1. `sudo dnf install ckb-next`
1. Headset Control (**headsetcontrol**)
    1. This can easily be installed by `sudo dnf install headsetcontrol`.
    1. Pipe the rules to an output file: `headsetcontrol -u > 70-headsets.rules`
    1. Copy the file to the rules location: `sudo mv 70-headsets.rules /etc/udev/rules.d/70-headsets.rules`
    1. Reload the udev config: `sudo udevadm control --reload-rules && sudo udevadm trigger`
1. Headset Charge Indicator: `https://github.com/centic9/headset-charge-indicator/`
1. Razer Drivers
    1. Follow instructions at: https://openrazer.github.io/#download.
    1. Replace the `Fedora_36` part with `Fedora_37`.
    1. Run `sudo gpasswd -a $USER plugdev`
    1. Install RazerGenie
