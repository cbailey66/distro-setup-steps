This is an attempt to capture my typical post-install steps when installing various distros. The goal is to ease fresh installs and document what I liked, didn't like, and what ended up working for my setup. The guides are generally only updated when I set up a new distro.

You are welcome to use these guides as a reference, if they are helpful. However, each guide is tailored to my personal preferences, use case, hardware, and peripherals.

Use at your own risk!!
