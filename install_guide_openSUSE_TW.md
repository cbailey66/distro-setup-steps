# Installer
1. Everything is extremely straightforward with the installer, so the defaults work for most everything
1. It's easy to use the guided setup, then use the expert setup **with current proposal** to make small changes, like mounting a specific partition
1. On the Summary Page:
    1. Select any additional software.
    1. Disable **Secure Boot**
    1. Disable **SSH**
    1. Block **SSH Port**

# Post-install Settings
## Initial Setup
* **Note: It may be a good idea to take single snapshots with YaST**
1. First Boot issues:
    1. In the grub menu, press the **E** key to edit the current parameters
    1. If installed through Ventoy, remove `rdinit=/vtoy/vtoy` from the kernel command line or the system will not boot!
    1. Add `modprobe.blacklist=nouveau` to the kernel command line until the nvidia drivres are installed.
    1. Press **Ctrl+X** to boot.
    1. Make these changes to `/etc/default/grub`
    1. Re-generate grub with `sudo grub2-mkconfig -o /boot/grub2/grub.cfg`
1. Remove The install disk repository, using yast (easiest) or:
    1. Use `sudo zypper lr -d` and figure out which repo number is the install disk.
    1. Use `sudo zypper mr -d -F #` to disable the repo, with `#` being the number from the first step.
    1. Use `sudo zypper rr #` to remove the repo, with `#` being the number from the first step.
    1. A refresh can bee done using `zypper -vvv ref -f`.

## Update the system
1. Use yast, or run:
    1. `sudo zypper ref`
    1. `sudo zypper dup`

## Add Wheel Group
* **Note** there could be a better way to do this, but these were used:
    * https://www.cyberciti.biz/faq/opensuse-install-sudo-to-execute-commands-as-root/
    * https://en.opensuse.org/SDB:Administer_with_sudo
1. Switch to root using `su`
1. Ensure the sudo configuration is installed with `zypper in sudo`
1. Run `visudo` uncommenting the **wheel** group (line `%wheel ALL=(ALL:ALL) ALL`)
1. Add user to the wheel group with `usermod -aG wheel <username>`
1. Run `visudo` and add `DISPLAY` and `XAUTHORITY` to the end of the `env_keep =` line.
1. Run `visudo` and comment out the following lines:
    1. `#Defaults targetpw    # ask for the password of the target user i.e. root`
    1. `#ALL ALL=(ALL) ALL # WARNING! Only use this together with 'Defaults targetpw'!`
1. Create a config file for the wheel group `visudo -f /etc/sudoers.d/wheel-users` and add the lines:
    1. `# Allow members of group wheel to execute any command`
    1. `%wheel ALL=(ALL) ALL`
1. Verify wheel was added with `grep ^wheel /etc/group`
    1. If it doesn't exist, run `groupadd wheel`
1. Verify your user is part of the wheel group with `id <username>`
1. Log out or restart for the changes to apply.

## Set up ZRAM
1. Install the systemd service with `sudo zypper in zram-generator`
1. Disable zswap by switching to root and running `echo 0 > /sys/module/zswap/parameters/enabled`
1. Set up the config file
    1. Copy the config file to the default location: `sudo cp /usr/share/doc/zram-generator/zram-generator.conf.example /usr/lib/systemd/zram-generator.conf`
    1. Edit the file and comment out everything for `[zram1]`
    1. Change `[zram0]`'s `zram-size` to `zram-size = min(ram, 8192)`
    1. Comment the other options to just use the Fedora defaults (above line)!
    1. Optionally, `compression-algorithm` can be set to use `zstd` instead of the default `lzo-rle`
    1. This setup is taken from the [zram-generator-defaults](https://aur.archlinux.org/packages/zram-generator-defaults) AUR package
1. Enable immediately with:
    1. `sudo systemctl daemon-reload`
    1. `sudo systemctl start /dev/zram0`
    1. Verify with `zramctl` or `swapon`
* **Source :** https://wiki.archlinux.org/title/Improving_performance#Zram_or_zswap
* **Source :** https://wiki.archlinux.org/title/Zram

## Enable SysReq Commands
1. Run `echo 'kernel.sysreq=1' | sudo tee /etc/sysctl.d/99-reisub.conf`
    1. Enables the SysReq key by default (`alt` + `Print Screen`)
    1. Allows resetting from a borked state by holdng `alt`, hitting the `Print Screen` key, then typing `REISUB`.
    1. Helpful mnemonic `Reboot Even If System Is Utterly Broken`
* **Source :** https://forum.endeavouros.com/t/tip-enable-magic-sysrq-key-reisub/7576

## Install Nvidia Drivers
1. Ensure the line `multiversion = provides:multiversion(kernel)` exists in `/etc/zypp/zypp.conf`
1. Add the Nvidia repo via yast (using the community repository option) or:
    1. `sudo zypper addrepo --refresh https://download.nvidia.com/opensuse/tumbleweed NVIDIA`
1. Install `x11-video-nvidiaG06` and the recommended packages through yast or `sudo zypper in x11-video-nvidiaG06`
1. Ensure that `nvidia-glG06` and any other packages are installed using yast or `sudo zypper se G06`
1. Wait until `modinfo -F version nvidia` shows that the drivers have been installed!
1. Install `suse-prime` if using a muxless Optimus laptop! This makes things way easier for dGPU mode as a desktop replacement, with external monitors.
1. Select dGPU-only mode on next boot with `sudo prime-select next-boot nvidia` (note that future boot uses the last setting!)
1. Reboot
1. If the system doesn't boot after restart (may not be necessary), try the following:
    * **Note: This was required on Fedora, but didn't seem to be on openSUSE Tumbleweed!**
    1. Add `modprobe.blacklist=nouveau rd.driver.blacklist=nouveau nvidia-drm.modeset=1` to GRUB_CMDLINE_LINUX in `/etc/default/grub`
    1. Regenerate grub with `sudo grub2-mkconfig -o /etc/grub2.cfg`
    * https://asus-linux.org/wiki/asusctl-install/
    * https://asus-linux.org/wiki/fedora-guide/
* https://en.opensuse.org/SDB:NVIDIA_drivers

# Personal Settings/Home Directory
## KDE Personal Preferences and Settings:
1. Set the global theme's color scheme to Breeze Dark
1. In **Input Devices -> Keyboard**: Enable numlock on plasma startup
1. In **Power Management -> Energy Saving**: Set up screen dim/display and power button settings. Personal preference:
    1. AC Power:    Dim = 10 min, Screen Off = 15 min, Power Button = Shutdown, Suspend: N/A
    1. Battery:     Dim = 5 min,  Screen Off = 10 min, Power Button = Shutdown, Suspend: Sleep after 15 min
    1. Low Battery: Dim = 1 min,  Screen Off = 2 min,  Power Button = Shutdown, Suspend: Sleep after 5 min
1. Enable double-click to open files/folders
    1. Go to Settings -> Workspace Behavior -> General Behavior
    1. Under `Clicking files or folders`, select the `Selects them` radio button
1. Update Dolphin to:
    1. **Show Hidden Files**
    1. **Sort Folders Before Files**
    1. **Sort Hidden Files Last**
1. Change task switcher/alt-tab options to use instructions
    1. Go to Settings -> Window Management -> Task Switcher
    1. Select `Large Icons` in the dropdown menu (second from the top under Visualization)
    1. Un-check `Show selected window` (top item under Visualization)
1. Stop and mask packagekit service with `sudo systemctl stop packagekit` and `sudo systemctl mask packagekit`
    1. Files should be installed with yast or zypper, not Discover.
    1. Yast and zypper better-resolve dependencies.
    1. This prevents accidentally re-enabling from dependencies.
    1. Discover can still be used for flatpaks!
    1. Source: https://www.reddit.com/r/openSUSE/comments/n3v2ie/comment/gws8ksb

## Misc/Personal Setup
1. Mount any external drives that weren't added during install and update fstab mount options
    1. Ensure that the `user` or `users` options are not present in fstab to prevent drives from being mounted with `noexec`
    1. For btrfs, `compress-force=zstd:1,ssd,space_cache=v2,commit=60` seems to be a good, recommended setup for NVMe SSDs.
    1. Discard is discouraged on NVMe SSDs (arch wiki), so a trim schedule should be set up.
    1. For ext4, `noatime,commit=60`
1. Add the home directory to snapper `snapper -c home create-config /home`
1. Make yast the default when opening RPM files from dolphin!
1. Make a symbolic link from the user GTK settings to global GTK settings for uniform appearance:
    1. `sudo ln -s /home/username/.gtkrc-2.0 /etc/gtk-2.0/gtkrc`
    1. `sudo ln -s /home/username/.config/gtk-3.0/settings.ini /etc/gtk-3.0/settings.ini`
    * **Source :** https://wiki.archlinux.org/title/GTK#Theme_not_applied_to_root_applications
1. Change headset/mic to **Pro Audio**
1. Create symbolic links for personal files (useful while distro-hopping!)
    1. Firefox Profile
        1. Remove the `/home/username/.mozilla/firefox/*-default-release` folder
        1. Run `ln -s <abs-path-to-profile> /home/username/.mozilla/firefox/*-default-release` using the same name as the previous step
        1. Example: `ln -s /mnt/storage/personal/ff_profile/ /home/beat/.mozilla/firefox/c9zggch2.default-release`
    1. Downloads
    1. Pictures
    1. Music
    1. Documents
    1. Any other folders (school/work/tools/repos/development/etc)
    1. **This allows easy debugging and fixing any modifications/issues with home and configuration files!!**
    * Ensure all monitors have the same refresh rate due to known bugs.
    * I encountered issues with the external displays, but seemed to be able to get them working eventually.
1. __TODO__ Set up a fstrim and btrfs-scrub schedule

## Setting up Konsole profile
1. Konsole has a bug where new profiles are deleted after created
1. To get around this, create a new profile, **click OK and nothing else**, then restart Konsole
1. Afterwards, modify the new profile as normal

# Add Other Repos
* Note: A lower-numbered priority (e.g., 90) allows new packages to be pulled from the new repo rather than the official repos. The official repos are added with the priority of 99 (higher number = lower priority).
* Note: There are 1-click installers here: https://www.opensuse-community.org/, but their performance hasn't always been the best!

## Packman
* The Packman repo is an external community repository that provides software that may be blacklisted by openSUSE and Germany's strict IP laws
    * These include: Codecs, Video Players, Multimedia, Networking, and Games
1. Add the entire repo with `sudo zypper ar -cfp 90 https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Tumbleweed/ packman`
    1. There are commands to only add a few items, if necessary.
1. Switch any installed packages to use Packman, if they exist, with `sudo zypper dup --from packman --allow-vendor-change`
    1. This is important for stability!!
    1. As an example, the packages modified on a fresh install were: Discord, FFMPEG, VLC, Mesa, libvulkan_intel, libvdpau_nouveau, and some codecs
* **Source:** http://packman.links2linux.org/
* **Source:** https://en.opensuse.org/Additional_package_repositories

## Open Build Service (OBS)
* The Open Build Service is similar to the AUR or COPR in that it supports community-maintained packages for installation that are not included in the official repos.
1. **OBS Package Installer (opi)** - `sudo zypper in opi`
    1. **opi** is a convenient OBS helper for searching, kind of similar to **yay** or **paru**, and also supports Packman
1. **Emulators:Wine repo** - `sudo zypper ar -cfp 90 https://download.opensuse.org/repositories/Emulators:/Wine/openSUSE_Tumbleweed/ Emulators:Wine`
    1. Community repo on the OBS that has the latest versions of Wine based on WineHQ
1. **games:tools repo** - `sudo zypper ar -cfp 90 http://download.opensuse.org/repositories/games:tools/openSUSE_Tumbleweed/ games:tools`
    1. Community repo on the OBS that has the latest versions of many gaming utilities like steam, lutris, goverlay, and emulators
1. Install codecs with `opi codecs`
* **Source:** https://en.opensuse.org/Portal:Build_Service
* **Source:** https://en.opensuse.org/Additional_package_repositories

## Flatpaks
1. Flatpaks should be enabled by default. If not, run `sudo zypper install flatpak`
1. Add flathub with `flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo`
1. Update flatpak with: `flatpak update`

# Software
Use yast or zypper to install these!

## KDE/Qt/DE Additions
1. **Plasma Rename Tool** - `sudo zypper in krename`
1. **Kvantum** - `sudo zypper in kvantum`
1. **Partition Manager** - `sudo zypper in partitionmanager`
1. **htop** - `sudo zypper in htop`
1. **neofetch** - `sudo zypper in neofetch`

## Shell Configuration and Helpers
1. **Fish** - `sudo zypper in fish`
1. **Thefuck** - `pip install thefuck`
1. **Starship** - `sudo zypper in starship`
1. **bat** - `sudo zypper in bat bat-bash-completion bat-fish-completion`

## Fonts
1. **Firacode Nerd Font**
    1. Download the latest release from github https://github.com/tonsky/FiraCode
    1. Extract the files
    1. Double click each TTF font
    1. Install to system
1. **Microsoft Fonts** - `sudo zypper in fetchmsttfonts`

## Media/Video Settings
1. **MPV** - `sudo zypper in mpv`
1. **OBS Studio** - `sudo zypper in obs-studio`
    1. Start up to configure webcam and settings
1. **GIMP** - `sudo zypper in gimp`

## Chat
1. **Discord** - `sudo zypper in discord`
    1. The version from Packman seems to be the latest

## Gaming
1. **Steam** - `sudo zypper in steam`
    1. Be sure to install from the `games:tools` repo!
1. **Lutris** - `sudo zypper in lutris`
    1. Be sure to install from the `games:tools` repo!
1. **ProtonUp-QT** - install from flatpak
1. **Mangohud & Goverlay** - `sudo zypper in goverlay-bin`
    1. Be sure to install from the `games:tools` repo!

## Wine
1. **Wine Staging** - `sudo zypper in wine-staging`
    1. This will uninstall previous wine installations
1. **Winetricks** - `sudo zypper in winetricks`

## Web Browsing
Note that some webapps (e.g., telehealth) might only run on Linux through Chrome/Chromium
1. **Ungoogled Chromium** - use flatpak

## Development/Text Editing
1. **Micro** - `sudo zypper in micro-editor`
1. **VSCodium** - `opi codium`
    1. Adds the github RPMs
    1. Ensure to keep the repo and set it to autorefresh
1. **meld** - `sudo zypper in meld`

## Other
1. **Flatseal** - install using flatkpak

## Paid software
1. **BeyondCompare**
    1. Import the GPG key `sudo rpm --import https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware`
    1. Refresh repos `sudo zypper ref`
    1. Install `zypper install https://www.scootersoftware.com/bcompare-4.4.4.27058.x86_64.rpm`
1. **NordVPN**
    1. Run `sh <(wget -qOnordvpn.sh https://downloads.nordcdn.com/apps/linux/install.sh)`
    1. Give the script execute permissions with `chmod +x nordvpn.sh`
    1. Run the script with `sudo ./nordvpn.sh`
    1. Add user to the nordvpn group with `sudo usermod -aG nordvpn $USER`
    1. Add the NordVPN Plasma Widget!

## Tools from asus-linux Project
* More information at: https://asus-linux.org/wiki/
1. Ensure the repo was previously added: `sudo zypper ar --priority 50 --refresh https://download.opensuse.org/repositories/home:/luke_nukem:/asus/openSUSE_Tumbleweed/ asus-linux`
1. Install the following software through yast or with `sudo zypper in asusctl asusctl-rog-gui`
    1. `asusctl` allows modifying Asus ROG laptop bios settings, fan curves, and keyboard controls!
    1. `asusctl-rog-gui` is a fantastic GUI for controlling fan profiles, performance profiles, and everything else that's offered by `asusctl` and `supergfxctl`!
1. Start the `asusd` service with `systemctl start asusd`
1. Remove `tlp` and any other software that may be maksing `power-profiles-daemon`.
    1. Install `power-profiles-daemon` instead to better manage power profiles!
1. Assuming the laptop doesn't support `nvidia.powerd`, mask the service with `sudo systemctl mask nvidia-powerd` - or deal with red-herring warnings.
* Debugging showed that with an Asus ROG G712 laptop, `supergfxctl` and hybrid mode caused performance issues - at least with the Nvidia driver. Using `suse-prime` and setting the dGPU with `sudo prime-select nvidia` works like a charm!

# Keyboard, Headset, Controllers, and Mice
## Set Up Xbox Controllers
1. Xone ([github](https://github.com/medusalix/xone))
    1. Install the dependencies with yast or `sudo zypper in curl dkms cabextract`
    1. Clone the repo from github with `git clone https://github.com/medusalix/xone.git`
    1. Install by running `sudo ./install.sh --release` in the `xone` folder
    1. Add firmware for the wireless dongle with `sudo /usr/local/bin/xone-get-firmware.sh --skip-disclaimer`
    1. Reboot the system.
1. Xpadneo ([github](https://github.com/atar-axis/xpadneo))
    1. Install the dependencies with yast or `sudo zypper in dkms make bluez kernel-devel kernel-source`
    1. Clone the repo from github with `git clone https://github.com/atar-axis/xpadneo.git`
    1. Install using DKMS with `sudo ./install.sh` in the `xpadneo` folder
    1. Reboot the system.

## Corsair Keyboard
1. Can directly install `ckb-next` with yast or `sudo zypper in ckb-next`
1. Start ckb-next and configure the keyboard as desired.

## Corsair Wireless Headset
1. Install `headsetcontrol` from [github](git clone https://github.com/Sapd/HeadsetControl)
    1. Install dependencies `sudo zypper in libhidapi-devel cmake`
    1. Clone the repo `git clone https://github.com/Sapd/HeadsetControl && cd HeadsetControl`
    1. Make the build directory `mkdir build && cd build`
    1. Run cmake `cmake ..`
    1. Build `make`
    1. Install globally `sudo make install`
    1. Pipe the rules to an output file: `headsetcontrol -u > 70-headsets.rules`
    1. Copy the file to the rules location: `sudo mv 70-headsets.rules /etc/udev/rules.d/70-headsets.rules`
    1. Reload the udev config: `sudo udevadm control --reload-rules && sudo udevadm trigger`
1. Install the headset notification tray icon/GUI from [github](https://github.com/centic9/headset-charge-indicator/)
    1. Clone the repo: `git clone https://github.com/centic9/headset-charge-indicator.git`
    1. Run `./install.sh` to auto-start

## Razer Drivers
1. Razer Drivers
    1. Follow instructions at: https://openrazer.github.io/#download.
    1. Provides a one-click installer!
    1. Run `sudo gpasswd -a $USER plugdev`
    1. Install RazerGenie

# General sources:
* **Source:** https://github.com/mikeroyal/SUSE-openSUSE-Guide/blob/main/README.md
* **Source:** https://averagelinuxuser.com/after-installing-opensuse/
