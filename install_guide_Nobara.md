# Installer:
* When booting the live image, and post-isntall, add `modprobe.blacklist=nouveau` in the grub menu
* Nouveau is known to cause errors, such as KDE icons missing!

# Post-install Settings
## Initial Setup
* **Note: It's a good idea to take snapshots with Timeshift for rollback throughout this process!**
* Ensure that `modprobe.blacklist=nouveau` is added to the kernel command line until the Nvidia drivers are installed
1. Update the system and restart
  1. Cancel any popups so that the system and welcome center get updated first
1. On reboot, add `modprobe.blacklist=nouveau` to the kernel command line
1. Accept and install Media Codecs from the popup
1. Accept and install the Nvidia Drivers from the popup
1. Reboot

## Enable SysReq Commands
1. Run `echo 'kernel.sysreq=1' | sudo tee /etc/sysctl.d/99-reisub.conf`
    1. Enables the SysReq key by default (`alt` + `Print Screen`)
    1. Allows resetting from a borked state by holdng `alt`, hitting the `Print Screen` key, then typing `REISUB`.
    1. Helpful mnemonic `Reboot Even If System Is Utterly Broken`
* **Source :** https://forum.endeavouros.com/t/tip-enable-magic-sysrq-key-reisub/7576

# Personal Settings/Home Directory
## Settings & Personal Preferences
1. Set up the desired monitor configuration and settings.
  * Ensure all monitors have the same refresh rate due to bugs - may be fixed in the future?
1. **Official:**
  1. Go to the **Look & Feel** tab of the Nobara Welcome app and select **Choose a Layout**
  1. Select **Traditional** or the desired layout
1. **KDE:**
  1. KDE: Go to **Settings -> Input Devices** and set the **NumLock** setting on startup to **Turn On**.
1. Update power settings for screen dim and turn off.
1. Update power button settings to turn off the computer.
1. Mount any external drives that weren't added during install and update fstab mount options
    1. For btrfs, `compress-force=zstd:1,ssd,space_cache=v2,commit=120` seems to be a good, recommended setup for NVMe SSDs.
    1. Discard is discouraged on NVMe SSDs (arch wiki), so a trim schedule should be set up.
    1. For ext4, `noatime,commit=120`

## Optional dnf.conf Settings
1. Open the file `/etc/dnf/dnf.conf` in a text editor (or `sudo nano /etc/dnf/dnf.conf`).
1. Set `max_parallel_downloads=10` for downloading packages in parallel (number can be lowered depending on internet connection). The default in Nobara is 6.
1. Set `defaultyes=True` to change the default option to Yes if desired.

## Misc/Personal Setup
1. Mount any external drives that weren't added during install and update fstab mount options
    1. Ensure that the `user` or `users` options are not present in fstab to prevent drives from being mounted with `noexec`
    1. For btrfs, `compress-force=zstd:1,ssd,space_cache=v2,commit=120` seems to be a good, recommended setup for NVMe SSDs.
    1. Discard is discouraged on NVMe SSDs (arch wiki), so a trim schedule should be set up.
    1. For ext4, `noatime,commit=120`
1. **KDE:**
  1. Make a symbolic link from the user GTK settings to global GTK settings for uniform appearance:
    1. `sudo ln -s /home/username/.gtkrc-2.0 /etc/gtk-2.0/gtkrc`
    1. `sudo ln -s /home/username/.config/gtk-3.0/settings.ini /etc/gtk-3.0/settings.ini`
    * **Source :** https://wiki.archlinux.org/title/GTK#Theme_not_applied_to_root_applications
1. **Official:**
  1. **TODO**
    * **Source :** https://wiki.archlinux.org/title/GTK#Theme_not_applied_to_root_applications
1. Change headset/mic to **Pro Audio**
1. Create symbolic links for personal files (useful while distro-hopping!)
  1. Firefox Profile
    1. Remove the `/home/username/.mozilla/firefox/*-default-release` folder
    1. Run `ln -s <abs-path-to-profile> /home/username/.mozilla/firefox/*-default-release` using the same name as the previous step
    1. Example: `ln -s /mnt/storage/personal/ff_profile/ /home/beat/.mozilla/firefox/c9zggch2.default-release`
  1. Downloads
  1. Pictures
  1. Music
  1. Documents
  1. Any other folders (school/work/tools/repos/development/etc)
1. Mount any external drives that weren't added during install and update fstab mount options

## Extension Preferences (Nobara Official's Gnome Extensions)
1. **Dash to Panel** 
  1. Under **Position**, change the main panel display to **Primary Monitor**
  1. Under **Position**, change the panel thickness to **40**
  1. Under **Style**, change the **App Icon Margin** to **1**
  1. Under **Style**, change the **App Icon Padding** to **5**
  1. Under **Behavior**, enable **Disable show overview on startup**
  1. Under **Fine-tune**, change the **Tray Font Size** to **15**
  1. Under **Fine-tune**, change the **Tray Item Padding** to **3**
  1. Under **Fine-tune**, change the **Status Icon Padding** to **3**
1. **Arcmenu** 
  1. Under **Menu -> ArcMenu Layout Tweaks**, change the **Default View** to **Categories List**
  1. Under **Menu -> Menu Visual Appearance**, change the **Height** to **575**
  1. Under **Menu -> Fine Tune**, change the **Category Icon Type** to **Full Color**
  1. Under **Menu -> Fine Tune**, change the **Shortcuts Icon Type** to **Full Color**
  1. Under **Menu Button**,cd change the **Icon Size** to **40**

# Install Software
Flatpaks and all Third Party Repos should already be set up.
## Pre-installed Software
1. **Steam**
1. **ProtonUp-Qt**
1. **Lutris**
1. **Mangohud & Goverlay** 
1. **Wine Stating**
1. **Winetricks**

## Nobara Welcome App
1. **OBS Studio**
  1. Be sure to start up the program and set up webcam!
1. **Discord**

## ASUS Tools
1. **asus-ctl** - `sudo dnf in asusctl`
1. **asus-ctl GUI** - Should be installed as a dependency, otherwise run `sudo dnf in asusctl-rog-gui`

## System Monitoring
1. **htop** - `sudo dnf in htop`

## Shell Configuration and Helpers
1. **Fish** - `sudo dnf in fish`
1. **Thefuck** - `sudo dnf in thefuck`
1. **Starship** - `sudo dnf copr enable atim/starship` then `sudo dnf in starship`

## Fonts
1. **Firacode Nerd Font** - `sudo dnf in fira-code-fonts`

## Media/Video
1. **MPV** - `sudo dnf in mpv`
1. **GIMP** - `sudo dnf in gimp`

## Web Browsing
Note that some webapps (e.g., telehealth) might only run on Linux through Chrome/Chromium
1. **Ungoogled Chromium** - Flatpak only

## Development/Text Editing
1. **Micro** - `sudo dnf in micro`
1. **VSCodium** - Flatpak only

## Other
1. **Flatseal** - use the flatpak
1. **Bottles** - `sudo dnf in bottles`

## Paid software
1. **BeyondCompare**
  1. Download the RPM with `wget https://www.scootersoftware.com/bcompare-4.4.4.27058.x86_64.rpm`
  1. Import the RPM with `sudo rpm --import https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware`
  1. Install with `sudo dnf install bcompare-4.4.4.27058.x86_64.rpm`
1. **NordVPN**
  1. Run `sh <(wget -qOnordvpn.sh https://downloads.nordcdn.com/apps/linux/install.sh)`
  1. Change the installer to executable `chmod +x nordvpn.sh`
  1. Install `sudo sh ./nordvpn.sh`
  1. Add user to the nordvpn group with `sudo usermod -aG nordvpn $USER`

# Keyboard, Headset, and Controllers
## Set Up Xbox Controllers
1. Run the **Install the XONE drivers** in the Nobara Welcome script.

## Set Up Headset, Mouse, and Keyboard
1. Corsair Keyboard (**ckb-next**)
  1. `sudo dnf install ckb-next`
1. Headset Control (**headsetcontrol**)
    1. This can easily be installed by `sudo dnf install headsetcontrol`.
    1. Pipe the rules to an output file: `headsetcontrol -u > 70-headsets.rules`
    1. Copy the file to the rules location: `sudo mv 70-headsets.rules /etc/udev/rules.d/70-headsets.rules`
    1. Reload the udev config: `sudo udevadm control --reload-rules && sudo udevadm trigger`
1. Install the headset notification tray icon/GUI (https://github.com/centic9/headset-charge-indicator/)
    1. Clone the repo: `git clone https://github.com/centic9/headset-charge-indicator.git`
    1. Run `./install.sh` to auto-start
1. Razer Drivers
    1. Follow instructions at: https://openrazer.github.io/#download.
    1. Run `sudo gpasswd -a $USER plugdev`
    1. Install RazerGenie
